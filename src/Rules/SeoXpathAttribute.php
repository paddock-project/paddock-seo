<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Rules;

use BadPixxel\Paddock\Seo\Models\AbstractSeoRule;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Check Value of Page DOM Node Attributes Using Xpath Filter
 */
class SeoXpathAttribute extends AbstractSeoRule
{
    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-xpath-attr";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "[SEO] Check Value of Page DOM Node Attribute Using Xpath Filter";
    }

    /**
     * {@inheritDoc}
     */
    public function verifyCrawler(Crawler $crawler): bool
    {
        /** @var string $xpath */
        $xpath = $this->getOption("xpath");
        /** @var string $attribute */
        $attribute = $this->getOption("attr");

        //====================================================================//
        // Extract Xpath Attribute
        try {
            if ($crawler->filterXPath($xpath)->count()) {
                $value = $crawler->filterXPath($xpath)->attr($attribute);
            } else {
                $value = null;
            }
        } catch (\Throwable $ex) {
            $value = null;
        }

        //====================================================================//
        // Verify Text Value
        return $this->verifyTextValue($value);
    }

    //====================================================================//
    // CONFIGURATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    protected static function configureRuleOptions(OptionsResolver $resolver): void
    {
        parent::configureRuleOptions($resolver);

        $resolver->setDefault("xpath", null);
        $resolver->setAllowedTypes("xpath", array('string'));
        $resolver->setDefault("attr", "content");
        $resolver->setAllowedTypes("attr", array('string'));
    }
}
