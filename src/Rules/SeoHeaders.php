<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Rules;

use BadPixxel\Paddock\Core\Rules as CoreRules;
use BadPixxel\Paddock\Seo\Models\AbstractSeoRule;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Check Headers of a DOM Element
 */
class SeoHeaders extends AbstractSeoRule
{
    const XPATH_FILTER = "//h1|//h2|//h3|//h4";

    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-headers";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "[SEO] Check Headers of an Html Page";
    }

    /**
     * {@inheritDoc}
     */
    public function verifyCrawler(Crawler $crawler): bool
    {
        //====================================================================//
        // Ensure Only One H1 is Found
        if (!$this->hasOnlyOneH1($crawler)) {
            return false;
        }
        //====================================================================//
        // Ensure Headers Are Descending
        if (!$this->hasSuitableHeadersOrder($crawler)) {
            return false;
        }

        return true;
    }

    //====================================================================//
    // VERIFICATIONS
    //====================================================================//

    /**
     * Ensure Only One H1 is Found
     */
    protected function hasOnlyOneH1(Crawler $crawler): bool
    {
        $count = $crawler->filterXPath("//h1")->count();
        //====================================================================//
        // Build Options Override
        $options = array("name" => "Number of H1 Header, a page only has one title!", "eq" => 1);
        //====================================================================//
        // Lower or Equal Value Required
        if (!$this->forward(CoreRules\isEqual::getCode(), $count, $options)) {
            return false;
        }

        return true;
    }

    /**
     * Ensure Only One H1 is Found
     */
    protected function hasSuitableHeadersOrder(Crawler $crawler): bool
    {
        $currentLevel = 0;
        //====================================================================//
        // Walk on Page Defined Headers
        /** @var \DOMElement $node */
        foreach ($crawler->filterXPath(self::XPATH_FILTER) as $node) {
            //====================================================================//
            // Extract Node Level
            $nodeLevel = (int) str_replace("h", "", $node->tagName);
            //====================================================================//
            // Node Level is Below Current
            if ($nodeLevel > $currentLevel) {
                if ($nodeLevel != ($currentLevel + 1)) {
                    $this->warning(sprintf(
                        "Header falls from H%s to H%s, See %s.",
                        $currentLevel,
                        $nodeLevel,
                        $node->textContent
                    ));

                    return false;
                }
            }
            $currentLevel = $nodeLevel;
        }

        return true;
    }
}
