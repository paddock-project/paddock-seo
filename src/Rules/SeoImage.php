<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Rules;

use BadPixxel\Paddock\Core\Models\Rules\AbstractRule;
use BadPixxel\Paddock\Core\Rules as CoreRules;
use BadPixxel\Paddock\Core\Rules\RulesHelper;
use Exception;
use Symfony\Component\DomCrawler\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Check an Image DOM Element
 */
class SeoImage extends AbstractRule
{
    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-image";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "[SEO] Check Contents of a DOM Image Element";
    }

    /**
     * @inheritDoc
     */
    public function verify($value): bool
    {
        //====================================================================//
        // WHATEVER => Verify Value
        if (!($image = $this->isImage($value))) {
            return false;
        }
        //====================================================================//
        // WHATEVER => Verify if Node is Excluded
        if ($this->isExcluded($image)) {
            return true;
        }

        $absoluteUri = $image->getUri();
        //====================================================================//
        // Not Empty Value Required
        if (!$this->forward(CoreRules\isNotEmpty::getCode(), $absoluteUri)) {
            return false;
        }
        //====================================================================//
        // Url Required
        if (!$this->forward(isUrl::getCode(), $absoluteUri)) {
            return false;
        }
        //====================================================================//
        // Alt Attribute Required
        if (!$this->hasAltAttribute($image)) {
            return false;
        }

        //====================================================================//
        // PERFORM ADVANCED CHECKS
        return $this->verifyAdvanced($image);
    }

    /**
     * Image Advanced Checks
     * - Image File Size
     *
     * @param Image $image
     *
     * @return bool
     */
    public function verifyAdvanced(Image $image): bool
    {
        //====================================================================//
        // Check if Advanced Checks are Requested
        $advancedChecks = array_filter(array_intersect_key(
            $this->getOptions(),
            array("gte" => null, "lte" => null)
        ));
        if (empty($advancedChecks)) {
            return true;
        }
        //====================================================================//
        // Fetch Image Size
        $imgSize = self::getRemoteFileSize($image);
        //====================================================================//
        // Build Options Override
        $options = array("name" => $image->getUri());
        //====================================================================//
        // Greater or Equal Value Required
        if (!$this->forward(CoreRules\isGreaterOrEqual::getCode(), $imgSize, $options)) {
            return false;
        }
        //====================================================================//
        // Lower or Equal Value Required
        if (!$this->forward(CoreRules\isLowerOrEqual::getCode(), $imgSize, $options)) {
            return false;
        }

        return true;
    }

    //====================================================================//
    // CONFIGURATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    protected static function configureRuleOptions(OptionsResolver $resolver): void
    {
        RulesHelper::addScalarOption($resolver, "ne");      // Image Url is not empty
        RulesHelper::addScalarOption($resolver, "url");     // Image Url is an Url
        RulesHelper::addScalarOption($resolver, "alt");     // Image has Alt Attribute
        RulesHelper::addScalarOption($resolver, "gte");     // Image File Size is GTE
        RulesHelper::addScalarOption($resolver, "lte");     // Image File Size is LTE

        // Exclude Node by Class
        $resolver->setDefault("exclude", array());
        $resolver->setAllowedTypes("exclude", array('array'));
    }

    //====================================================================//
    // VERIFICATIONS
    //====================================================================//

    /**
     * Ensure Received Value is Valid DOM Crawler
     *
     * @param mixed $value
     *
     * @return null|Image
     */
    protected function isImage($value): ?Image
    {
        if (!is_object($value) || !$value instanceof Image) {
            $this->emergency(sprintf(
                "%s Rule Only check %s, %s received.",
                get_class($this),
                Image::class,
                is_object($value) ? get_class($value) : print_r($value, true)
            ));

            return null;
        }

        //====================================================================//
        // Detect Images from Data Sources
        if (empty($value->getNode()->getAttribute("src"))) {
            if (!empty($value->getNode()->getAttribute("data-src"))) {
                $value->getNode()->setAttribute(
                    "src",
                    $value->getNode()->getAttribute("data-src")
                );
            }
        }

        return $value;
    }

    /**
     * Ensure Received Value is Valid DOM Crawler
     *
     * @param Image $image
     *
     * @return bool
     */
    protected function isExcluded(Image $image): bool
    {
        /** @var string[] $excluded */
        $excluded = $this->getOption("exclude");

        if (empty($excluded)) {
            return false;
        }
        $nodeClass = " ".$image->getNode()->getAttribute("class")." ";
        foreach ($excluded as $excludedClass) {
            if (false !== strpos($nodeClass, " ".$excludedClass." ")) {
                return true;
            }
        }

        return false;
    }

    /**
     * Ues CURL to detect Remote File Size
     *
     * @param Image $image
     *
     * @return int
     */
    protected static function getRemoteFileSize(Image $image): int
    {
        static $cache;

        $uri = $image->getUri();

        if (!isset($cache[$uri])) {
            $result = curl_init($image->getUri());
            if (!$result) {
                return $cache[$uri] = 0;
            }

            //====================================================================//
            // TODO Uncomment this Allow Loading of webp Sizes
            //            $request = array();
            //            $request[] = 'Connection: keep-alive';
            //            $request[] = 'Pragma: no-cache';
            //            $request[] = 'Cache-Control: no-cache';
            //            $request[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
            //            $request[] = 'DNT: 1';
            //            $request[] = 'Accept-Encoding: gzip, deflate';
            //            $request[] = 'Accept-Language: en-US,en;q=0.8';
            //            curl_setopt($result, CURLOPT_HTTPHEADER, $request);

            curl_setopt($result, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($result, CURLOPT_HEADER, true);
            curl_setopt($result, CURLOPT_NOBODY, true);
            curl_setopt($result, CURLOPT_POST, false);
            curl_setopt($result, CURLOPT_FOLLOWLOCATION, true);
            curl_exec($result);
            $fileSize = curl_getinfo($result, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            curl_close($result);

            $cache[$uri] = (int) $fileSize;
        }

        return $cache[$uri];
    }

    /**
     * Ensure Image has a Valid Alt Attribute
     *
     * @param Image $image
     *
     * @throws Exception
     *
     * @return bool
     */
    private function hasAltAttribute(Image $image): bool
    {
        $result = 1;
        foreach (RulesHelper::getLevelCriteria($this->getOptions(), "alt") as $level => $constraint) {
            if (!$constraint) {
                continue;
            }
            $value = $image->getNode()->getAttribute("alt");
            if (!empty($value)) {
                $this->info($this->getValueName()." is Defined", array($value));

                continue;
            }

            $result &= $this->log($level, $this->getValueName()." is not Defined", array($image->getUri()));
            if (!$result) {
                return false;
            }
        }

        return (bool) $result;
    }
}
