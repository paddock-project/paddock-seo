<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Rules;

use BadPixxel\Paddock\Core\Models\Rules\AbstractRule;
use BadPixxel\Paddock\Core\Rules\RulesHelper;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Check Value is a valid Link
 */
class isUrl extends AbstractRule
{
    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-link";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "[SEO] Check Scalar Value is a Valid Link";
    }

    /**
     * @inheritDoc
     */
    public function verify($value): bool
    {
        $result = 1;
        foreach (RulesHelper::getLevelCriteria($this->getOptions(), "url") as $level => $constraint) {
            if (!$constraint) {
                continue;
            }
            if (!$this->isUrl($value, $level)) {
                return false;
            }
            $this->info($this->getValueName()." is a valid Url", array($value));
        }

        return (bool) $result;
    }

    /**
     * Verify Url
     *
     * @param mixed $value
     * @param int   $level
     *
     * @return bool
     */
    public function isUrl($value, int $level): bool
    {
        //====================================================================//
        // Safety Check
        if (!is_string($value)) {
            $this->log($level, $this->getValueName()." is not a string", array($value));

            return false;
        }
        //====================================================================//
        // Parse Url
        $parsed = parse_url($value);
        //====================================================================//
        // Verify Host & Path
        if (empty($parsed['host']) && empty($parsed['path'])) {
            $this->log($level, $this->getValueName()." is not a valid Url", array($value));

            return false;
        }
        //====================================================================//
        // Verify Schema
        if (!empty($parsed['scheme']) && !in_array($parsed['scheme'], array("http", "https"), true)) {
            $this->log($level, $this->getValueName()." has invalid Url Scheme", array($value));

            return false;
        }

        return true;
    }

    //====================================================================//
    // CONFIGURATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    protected static function configureRuleOptions(OptionsResolver $resolver): void
    {
        RulesHelper::addScalarOption($resolver, "url");     // Value is an Url
    }
}
