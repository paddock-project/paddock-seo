<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Suites;

use BadPixxel\Paddock\Seo\Models\ConfigurableSeoRule;

/**
 * THE Default Pages Seo Validation Test Suite
 */
class DefaultSuite extends ConfigurableSeoRule
{
    /**
     * List of Page Xpath Contents Constraints Files
     *
     * @var string[]
     */
    protected static array $xpathContentsFiles = array(
        __DIR__."/../Resources/constraints/header/default/xpath-tags-core.yml"
    );

    /**
     * {@inheritDoc}
     */
    protected static array $xpathAttrFiles = array(
        __DIR__."/../Resources/constraints/header/default/xpath-attr-core.yml",
        __DIR__."/../Resources/constraints/header/default/xpath-attr-meta.yml",
        __DIR__."/../Resources/constraints/header/default/xpath-attr-opengraph.yml",
    );

    /**
     * {@inheritDoc}
     */
    protected static array $imagesFiles = array(
        __DIR__."/../Resources/constraints/images/default/img-core.yml",
    );

    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-default";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "Default Pages Seo Validation Test Suite";
    }
}
