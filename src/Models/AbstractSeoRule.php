<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Models;

use BadPixxel\Paddock\Core\Models\Rules\AbstractRule;
use BadPixxel\Paddock\Core\Rules as CoreRules;
use BadPixxel\Paddock\Core\Rules\RulesHelper;
use BadPixxel\Paddock\Seo\Rules as SeoRules;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractSeoRule extends AbstractRule
{
    /**
     * {@inheritDoc}
     */
    final public function verify($value): bool
    {
        //====================================================================//
        // WHATEVER => Verify Value
        if (!($client = $this->isCrawler($value))) {
            return false;
        }

        //====================================================================//
        // Execute Rule Verifications
        return $this->verifyCrawler($client);
    }

    /**
     * Verify Client Response Crawler
     *
     * @param Crawler $crawler
     *
     * @return bool
     */
    protected function verifyCrawler(Crawler $crawler): bool
    {
        return $this->emergency(sprintf("No Verifications defined on %s", get_class($this)));
    }

    /**
     * Verify a Text Value
     *
     * @param null|string $value
     *
     * @return bool
     */
    protected function verifyTextValue(?string $value): bool
    {
        $length = strlen((string) $value);
        //====================================================================//
        // Empty Value Required
        if (!$this->forward(CoreRules\isEmpty::getCode(), $value)) {
            return false;
        }
        //====================================================================//
        // Not Empty Value Required
        if (!$this->forward(CoreRules\isNotEmpty::getCode(), $value)) {
            return false;
        }
        //====================================================================//
        // Equal Value Required
        if (!$this->forward(CoreRules\isEqual::getCode(), $value)) {
            return false;
        }
        //====================================================================//
        // Url Required
        if (!$this->forward(SeoRules\isUrl::getCode(), $value)) {
            return false;
        }
        //====================================================================//
        // Same Value Required
        if (!$this->forward(CoreRules\isSame::getCode(), $value)) {
            return false;
        }
        //====================================================================//
        // Greater Value Required
        if (!$this->forward(CoreRules\isGreater::getCode(), $length)) {
            return false;
        }
        //====================================================================//
        // Greater or Equal Value Required
        if (!$this->forward(CoreRules\isGreaterOrEqual::getCode(), $length)) {
            return false;
        }
        //====================================================================//
        // Lower Value Required
        if (!$this->forward(CoreRules\isLower::getCode(), $length)) {
            return false;
        }
        //====================================================================//
        // Lower or Equal Value Required
        if (!$this->forward(CoreRules\isLowerOrEqual::getCode(), $length)) {
            return false;
        }

        return true;
    }

    /**
     * Ensure Received Value is Valid DOM Crawler
     *
     * @param mixed $value
     *
     * @return null|Crawler
     */
    protected function isCrawler($value): ?Crawler
    {
        if (!is_object($value) || !$value instanceof Crawler) {
            $this->emergency(sprintf(
                "%s Rule Only check %s, %s received.",
                get_class($this),
                Crawler::class,
                is_object($value) ? get_class($value) : print_r($value, true)
            ));

            return null;
        }

        return $value;
    }

    //====================================================================//
    // CONFIGURATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    protected static function configureRuleOptions(OptionsResolver $resolver): void
    {
        RulesHelper::addScalarOption($resolver, "empty");   // Value is empty
        RulesHelper::addScalarOption($resolver, "ne");      // Value is not empty
        RulesHelper::addScalarOption($resolver, "eq");      // Value is equal
        RulesHelper::addScalarOption($resolver, "same");    // Value is same
        RulesHelper::addScalarOption($resolver, "gt");      // Value is greater
        RulesHelper::addScalarOption($resolver, "gte");     // Value is greater or equal
        RulesHelper::addScalarOption($resolver, "lt");      // Value is lower
        RulesHelper::addScalarOption($resolver, "lte");     // Value is lower or equal

        RulesHelper::addScalarOption($resolver, "url");     // Value is an Url

        //====================================================================//
        // Exclude Node by Class
        $resolver->setDefault("exclude", array());
        $resolver->setAllowedTypes("exclude", array('array'));
    }
}
