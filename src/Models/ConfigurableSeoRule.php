<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Models;

use BadPixxel\Paddock\Seo\Rules;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Configurable Page Seo Validator
 */
abstract class ConfigurableSeoRule extends AbstractSeoRule
{
    /**
     * List of Page Xpath Contents Constraints Files
     *
     * @var string[]
     */
    protected static array $xpathContentsFiles = array();

    /**
     * List of Page Xpath Attributes Constraints Files
     *
     * @var string[]
     */
    protected static array $xpathAttrFiles = array();

    /**
     * Images Files Constraints File
     *
     * @var string[]
     */
    protected static array $imagesFiles = array();

    //====================================================================//
    // PAGE RULES DEFINITION
    //====================================================================//

    /**
     * Get List of Page Xpath Contents Rules
     *
     * Applied rule: SeoXpathContents
     *
     * @return array<array<string, scalar>>
     */
    protected function getXpathContentsConstraints(): array
    {
        return $this->loadConstraintsFromFiles(static::$xpathContentsFiles);
    }

    /**
     * Get List of Page Xpath Attribute Rules
     *
     * Applied rule: SeoXpathAttributes
     *
     * @return array<array<string, scalar>>
     */
    protected function getXpathAttributeConstraints(): array
    {
        return $this->loadConstraintsFromFiles(static::$xpathAttrFiles);
    }

    /**
     * Get List of Page Images Files Rules
     *
     * Applied rule: SeoXpathAttributes
     *
     * @return array<array<string, scalar>>
     */
    protected function getImagesConstraints(): array
    {
        return $this->loadConstraintsFromFiles(static::$imagesFiles);
    }

    //====================================================================//
    // PAGE VERIFICATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    final protected function verifyCrawler(Crawler $crawler): bool
    {
        $errors = 0;
        //====================================================================//
        // Xpath Contents Verifications
        foreach ($this->getXpathContentsConstraints() as $constraint) {
            if (!$this->forward(Rules\SeoXpathContents::getCode(), $crawler, $constraint)) {
                $errors++;
            }
        }
        //====================================================================//
        // Xpath Attributes Verifications
        foreach ($this->getXpathAttributeConstraints() as $constraint) {
            if (!$this->forward(Rules\SeoXpathAttribute::getCode(), $crawler, $constraint)) {
                $errors++;
            }
        }
        //====================================================================//
        // Header Verifications
        if (!$this->forward(Rules\SeoHeaders::getCode(), $crawler)) {
            $errors++;
        }
        //====================================================================//
        // Images Verifications
        foreach ($crawler->filterXPath("//img")->images() as $imgNode) {
            foreach ($this->getImagesConstraints() as $constraint) {
                if (!$this->forward(Rules\SeoImage::getCode(), $imgNode, $constraint)) {
                    $errors++;
                }
            }
        }

        return empty($errors);
    }

    /**
     * Load a List of Constraints from Yaml Files
     *
     * @param string[] $files
     *
     * @return array<array<string, scalar>>
     */
    protected function loadConstraintsFromFiles(array $files): array
    {
        $constraints = array();
        //====================================================================//
        // Walk on Files
        foreach ($files as $file) {
            $file = realpath($file);
            //====================================================================//
            // Safety Check
            if (!$file || !is_file($file) || !is_readable($file)) {
                $this->emergency(sprintf("DOM Contraints File %s was not found", $file));

                continue;
            }

            //====================================================================//
            // Parse Yaml Contents
            try {
                /** @var null|array<string, mixed> $values */
                $values = Yaml::parseFile($file, Yaml::PARSE_EXCEPTION_ON_INVALID_TYPE);
                $values = is_array($values) ? $values : array();
            } catch (ParseException $exception) {
                $this->emergency($exception->getMessage());

                continue;
            }
            /** @var array<array<string, scalar>> $constraints */
            $constraints = array_merge($constraints, $values);
        }

        return $constraints;
    }
}
