<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Sitemap;

class SitemapLoader
{
    private static array $cache = array();

    /**
     * @param string $path
     *
     * @return null|string[]
     */
    public static function load(string $path): ?array
    {
        //====================================================================//
        // Already Parsed
        if (isset(self::$cache[$path])) {
            return self::$cache[$path];
        }

        //====================================================================//
        // Extract Sitemap Urls
        return self::$cache[$path] = self::urlsFromPath($path);
    }

    /**
     * Load File & Convert Raw Xml to Php Array
     *
     * @param string $path
     *
     * @return string[]
     */
    private static function urlsFromPath(string $path): array
    {
        //====================================================================//
        // Load Sitemap File
        $sitemaps = self::loadFromPath($path);
        if (empty($sitemaps)) {
            return array();
        }
        $urls = array();
        //====================================================================//
        // Walk on Child Sitemaps
        foreach ($sitemaps['sitemap'] ?? array() as $sitemap) {
            $sitemapUrl = $sitemap["loc"] ?? null;
            if (!empty($sitemapUrl) && is_string($sitemapUrl)) {
                $urls = array_merge($urls, self::urlsFromPath($sitemapUrl));
            }
        }
        //====================================================================//
        // Walk on Urls
        foreach ($sitemaps['url'] ?? array() as $url) {
            $location = $url["loc"] ?? null;
            if (!empty($location) && is_string($location)) {
                $urls[] = $location;
            }
        }

        return $urls;
    }

    /**
     * Load File & Convert Raw Xml to Php Array
     *
     * @param string $path
     *
     * @return null|array<string, array>
     */
    private static function loadFromPath(string $path): ?array
    {
        //====================================================================//
        // Load Sitemap Raw File
        $rawXml = file_get_contents($path);
        if (empty($rawXml)) {
            return null;
        }
        //====================================================================//
        // Load Xml File
        $xml = simplexml_load_string($rawXml);
        if (empty($xml)) {
            return null;
        }
        //====================================================================//
        // Convert Xml to Php Arrays
        $array = json_decode((string) json_encode((array) $xml), true);
        if (empty($array) || !is_array($array)) {
            return null;
        }

        return $array;
    }
}
