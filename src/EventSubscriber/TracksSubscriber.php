<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\EventSubscriber;

use BadPixxel\Paddock\Core\Models\Tracks\AbstractTracksSubscriber;
use BadPixxel\Paddock\Seo\Tracks;

class TracksSubscriber extends AbstractTracksSubscriber
{
    /**
     * {@inheritDoc}
     */
    public function getStaticTracks(): array
    {
        return array(
            Tracks\SeoSitemapChecker::class => array('all' => true),
            Tracks\SeoUrlChecker::class => array('all' => true),
        );
    }
}
