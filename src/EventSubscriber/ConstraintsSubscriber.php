<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\EventSubscriber;

use BadPixxel\Paddock\Core\Events\GetConstraintsEvent;
use BadPixxel\Paddock\Seo\Rules;
use BadPixxel\Paddock\Seo\Suites;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConstraintsSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            GetConstraintsEvent::class => "registerConstraints",
        );
    }

    /**
     * @param GetConstraintsEvent $event
     */
    public function registerConstraints(GetConstraintsEvent $event): void
    {
        //====================================================================//
        // Page DOM Unit Checks
        $event->add(Rules\isUrl::class);
        $event->add(Rules\SeoXpathAttribute::class);
        $event->add(Rules\SeoXpathContents::class);
        $event->add(Rules\SeoHeaders::class);
        $event->add(Rules\SeoImage::class);

        //====================================================================//
        // Page DOM General Checks Sequences
        $event->add(Suites\DefaultSuite::class);
        $event->add(Suites\AdvancedSuite::class);
    }
}
