<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Iterators;

use BadPixxel\Paddock\Seo\Collector\UrlCollector;
use Exception;
use Iterator;

/**
 * Paddock Iterator for Site Urls Verifications
 */
class UrlsIterator implements Iterator
{
    /**
     * @var string
     */
    private string $rule;

    /**
     * @var Iterator
     */
    private Iterator $iterator;

    /**
     * Constructor
     */
    public function __construct(array $urls, string $rule = "seo-suite-default")
    {
        $this->iterator = new \ArrayIterator($urls);
        $this->rule = $rule;
    }

    /**
     * @inheritDoc
     */
    public function current(): array
    {
        //====================================================================//
        // Build Integrity Check Rule
        /** @var string $current */
        $current = $this->iterator->current();

        return $this->toRule($current);
    }

    /**
     * @inheritDoc
     */
    public function next(): void
    {
        $this->iterator->next();
    }

    /**
     * @inheritDoc
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
        return $this->iterator->key();
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    /**
     * @inheritDoc
     */
    public function rewind(): void
    {
        $this->iterator->rewind();
    }

    /**
     * Build Rule from Current Query Iterator Results
     *
     * @param string $current
     *
     * @throws Exception
     *
     * @return array
     */
    private function toRule(string $current)
    {
        return array(
            "key" => (string) $current,
            "rule" => $this->rule,
            "collector" => UrlCollector::getCode(),
        );
    }
}
