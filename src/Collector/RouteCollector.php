<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Collector;

use BadPixxel\Paddock\Core\Collector\AbstractCollector;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\HttpKernelBrowser;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Page Contents & Dom Collector
 */
class RouteCollector extends AbstractCollector
{
    /**
     * @var KernelInterface
     */
    private KernelInterface $kernel;

    /**
     * @var RouterInterface
     */
    private RouterInterface $router;

    /**
     * Collector Constructor.
     *
     * @param CacheInterface  $paddockCollectors
     * @param KernelInterface $kernel
     */
    public function __construct(
        CacheInterface $paddockCollectors,
        KernelInterface $kernel,
        RouterInterface $router
    ) {
        parent::__construct($paddockCollectors);
        $this->kernel = $kernel;
        $this->router = $router;
    }

    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-route";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "[SEO] Collect Page DOM Contents by Route";
    }

    //====================================================================//
    // CONFIGURATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        //====================================================================//
        // Request Method
        $resolver->setDefault("method", "GET");
        $resolver->setAllowedTypes("method", array("string"));
        //====================================================================//
        // Request Parameters
        $resolver->setDefault("parameters", array());
        $resolver->setAllowedTypes("parameters", array("array"));
    }

    //====================================================================//
    // DOCTRINE MANAGER CHECKS
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    protected function get(string $key): ?Crawler
    {
        //====================================================================//
        // Create Http Client
        $client = $this->createClient();
        /** @var string $method */
        $method = $this->getOption("method");
        /** @var string[] $parameters */
        $parameters = $this->getOption("parameters");
        //====================================================================//
        // Generate Url
        $uri = $this->router->generate($key, $parameters);
        //====================================================================//
        // Execute Request
        $crawler = $client->request($method, $uri);

        return $client->getResponse()->isSuccessful() ? $crawler : null;
    }

    /**
     * Ensure Sf Client is Loaded.
     */
    private function createClient(): HttpKernelBrowser
    {
        return new HttpKernelBrowser($this->kernel);
        ;
    }
}
