<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Collector;

use BadPixxel\Paddock\Core\Collector\AbstractCollector;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Collect Page DOM Contents by Url
 */
class UrlCollector extends AbstractCollector
{
    /**
     * Collector Constructor.
     *
     * @param CacheInterface $paddockCollectors
     */
    public function __construct(CacheInterface $paddockCollectors)
    {
        parent::__construct($paddockCollectors);
    }

    //====================================================================//
    // DEFINITION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public static function getCode(): string
    {
        return "seo-url";
    }

    /**
     * {@inheritDoc}
     */
    public static function getDescription(): string
    {
        return "[SEO] Collect Page DOM Contents by Url";
    }

    //====================================================================//
    // CONFIGURATION
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        //====================================================================//
        // Request Query parameters
        $resolver->setDefault("query", array());
        $resolver->setAllowedTypes("query", array("array"));
    }

    //====================================================================//
    // DOCTRINE MANAGER CHECKS
    //====================================================================//

    /**
     * {@inheritDoc}
     */
    protected function get(string $key): ?Crawler
    {
        //====================================================================//
        // Build Query String
        /** @var string[] $query */
        $query = $this->getOption("query");
        if (!empty($query)) {
            $key .= "?".http_build_query($query);
        }
        //====================================================================//
        // Execute Request
        $rawContents = file_get_contents($key);
        if (empty($rawContents) || !is_string($rawContents)) {
            return null;
        }

        return new Crawler($rawContents, $key);
    }
}
