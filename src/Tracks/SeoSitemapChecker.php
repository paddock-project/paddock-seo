<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Seo\Tracks;

use BadPixxel\Paddock\Core\Loader\EnvLoader;
use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;
use BadPixxel\Paddock\Core\Services\RulesManager;
use BadPixxel\Paddock\Seo\Collector\UrlCollector;
use BadPixxel\Paddock\Seo\Iterators\UrlsIterator;
use BadPixxel\Paddock\Seo\Sitemap\SitemapLoader;
use BadPixxel\Paddock\Seo\Suites\DefaultSuite;
use Iterator;

class SeoSitemapChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("seo-sitemap-url");

        //====================================================================//
        // Track Configuration
        $this->enabled = !empty(EnvLoader::get("PADDOCK_SEO_SITEMAP"));
        $this->description = "[SEO] Via Sitemaps";
        $this->collector = UrlCollector::getCode();
    }

    /**
     * Get Rules / Constraints
     *
     * @return array[]
     */
    public function getRules(): array
    {
        return array();
    }

    /**
     * Get Rules / Constraints Iterator
     *
     * @return Iterator<array>
     */
    public function getRulesIterator(): Iterator
    {
        //====================================================================//
        // Detect Test Suite Overrides
        /** @var null|string $testSuite */
        $testSuite = EnvLoader::get("PADDOCK_SEO_RULE");
        if (!$testSuite || !RulesManager::getInstance()->has($testSuite)) {
            $testSuite = DefaultSuite::getCode();
        }

        return new UrlsIterator($this->getUrls(), $testSuite);
    }

    /**
     * Get Rules / Constraints Count
     *
     * @return int
     */
    public function getRulesCount(): int
    {
        return count($this->getUrls());
    }

    /**
     * Get List of Urls to Check
     *
     * @return string[]
     */
    private function getUrls(): array
    {
        //====================================================================//
        // Load Sitemap Urls from Path
        /** @var null|string $sitemapUrl */
        $sitemapUrl = EnvLoader::get("PADDOCK_SEO_SITEMAP");
        if (!$sitemapUrl) {
            return array();
        }
        //====================================================================//
        // Load Sitemap Urls from Path
        $urls = SitemapLoader::load($sitemapUrl);

        return $urls ?? array();
    }
}
